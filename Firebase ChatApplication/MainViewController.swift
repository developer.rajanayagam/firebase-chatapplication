//
//  MainViewController.swift
//  Firebase ChatApplication
//
//  Created by raja vijaya kumar on 10/05/19.
//  Copyright © 2019 guvi. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var uidLabel: UILabel!
    
    @IBOutlet weak var logoutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        loadValues()
        customizeLogoutButton()
    }
    func loadValues() {
        self.nameLabel.text = UserServices.currentUser.model?.name
        self.emailLabel.text = UserServices.currentUser.model?.email
        self.uidLabel.text = UserServices.currentUser.model?.id
    }
    func customizeLogoutButton() {
        self.logoutButton.layer.cornerRadius = self.logoutButton.frame.height / 2
        self.logoutButton.clipsToBounds = true
    }
    
    @IBAction func logoutButtonAction(_ sender: Any) {
        
        AuthenticationService.shared.signout { [weak self] (success) in
            
            guard let `self` = self else {
                return
            }
            
            guard success else {
                AlertController.showStaticAlert(withMessage: "Cannot logout user", viewController: self)
                return
            }
            
            self.navigationController?.popViewController(animated: true)
        }
    }
}
