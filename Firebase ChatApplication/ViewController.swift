//
//  ViewController.swift
//  Firebase ChatApplication
//
//  Created by raja vijaya kumar on 10/05/19.
//  Copyright © 2019 guvi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        customizeLoginButton()
        customizeSignupButton()
    }
    
    func customizeLoginButton() {
        self.loginButton.layer.cornerRadius = self.loginButton.frame.height / 2
        self.loginButton.clipsToBounds = true
    }
    
    func customizeSignupButton() {
        self.signupButton.layer.cornerRadius = self.loginButton.frame.height / 2
        self.signupButton.clipsToBounds = true
    }

    @IBAction func loginButtonAction(_ sender: Any) {
        
        
        AuthenticationService.shared.login(withUserName: usernameField.text ?? "", password: passwordField.text ?? "") { [weak self] (success, user, error) in
            
            guard let `self` = self else {
                return
            }
            
            guard error == nil else {
                AlertController.showStaticAlert(withMessage: error!.localizedDescription, viewController: self)
                print("Error: \(error.debugDescription)")
                return
            }
            
            UserServices.currentUser.model = user
            self.loadMainView()
            
        }
    }
    
    @IBAction func signupAction(_ sender: Any) {
        
        let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        signupVC.delegate = self
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
    
    func loadMainView() {
        let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    
}

extension ViewController: SignupDelegate {
    
    func userSignedIn() {
        loadMainView()
    }
}

// To dismiss the keyboard
extension ViewController {
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.view.endEditing(true)
    }
}

