//
//  UserModel.swift
//  Firebase ChatApplication
//
//  Created by raja vijaya kumar on 10/05/19.
//  Copyright © 2019 guvi. All rights reserved.
//

import Foundation

struct UserModel {
    var name: String
    var email: String
    var id: String
}
