//
//  AuthenticationService.swift
//  Firebase ChatApplication
//
//  Created by raja vijaya kumar on 10/05/19.
//  Copyright © 2019 guvi. All rights reserved.
//

import Foundation
import FirebaseAuth

class AuthenticationService {
    typealias UserCompletion = ((Bool, UserModel?, Error?) -> Void)
    typealias Completion = ((Bool) -> Void)
    
    static let shared = AuthenticationService()
    
    private init() { }
    
    func login(withUserName userName: String, password: String, completion: UserCompletion?) {
        
        Auth.auth().signIn(withEmail: userName, password: password) { [weak self] (result, error) in
            
            guard let `self` = self else {
                return
            }
            
            guard error == nil else {
                completion?(false, nil, error)
                return
            }
            
            guard let user = result?.user else {
                completion?(false, nil, NSError(domain: "Unexpected Error: Result and the user is nil", code: 100, userInfo: nil))
                return
            }
            
            completion?(true, UserModel(name: user.displayName ?? "(no display name)", email: user.email ?? "(no email)", id: user.uid), nil)
        }
    }
    
    func signup(withUserName userName: String, password: String, completion: UserCompletion?) {
        
        Auth.auth().createUser(withEmail: userName, password: password) { [weak self] (result, error) in
            
            guard let `self` = self else {
                return
            }
            
            guard error == nil else {
                completion?(false, nil, error)
                return
            }
            
            guard let user = result?.user else {
                completion?(false, nil, NSError(domain: "Unexpected Error: Result and the user is nil", code: 100, userInfo: nil))
                return
            }
            
            completion?(true, UserModel(name: user.displayName ?? "(no display name)", email: user.email ?? "(no email)", id: user.uid), nil)
        }
        
    }
    
    func signout(completion: Completion?) {
        
        do {
            try Auth.auth().signOut()
            UserServices.currentUser.model = nil
            completion?(true)
        } catch let error {
            completion?(false)
        }
        
    }
}
