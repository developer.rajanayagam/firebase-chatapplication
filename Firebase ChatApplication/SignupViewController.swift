//
//  SignupViewController.swift
//  Firebase ChatApplication
//
//  Created by raja vijaya kumar on 10/05/19.
//  Copyright © 2019 guvi. All rights reserved.
//

import UIKit

protocol SignupDelegate {
    func userSignedIn()
}

class SignupViewController: UIViewController {

    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signupButton: UIButton!
    
    var delegate: SignupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        customizeSignupButton()
    }
    
    func customizeSignupButton() {
        
        self.signupButton.layer.cornerRadius = self.signupButton.frame.height / 2
        self.signupButton.clipsToBounds = true
    }
    
    @IBAction func signupButtonAction(_ sender: Any) {
        
        AuthenticationService.shared.signup(withUserName: usernameField.text ?? "", password: passwordField.text ?? "") { [weak self] (success, user, error) in
            
            guard let `self` = self else {
                return
            }
            
            guard error == nil else {
                AlertController.showStaticAlert(withMessage: error!.localizedDescription, viewController: self)
                return
            }
            
            UserServices.currentUser.model = user
            self.navigationController?.popViewController(animated: true)
            self.delegate?.userSignedIn()
        }
        
        
    }
    

}
