//
//  Utils.swift
//  Firebase ChatApplication
//
//  Created by raja vijaya kumar on 10/05/19.
//  Copyright © 2019 guvi. All rights reserved.
//

import Foundation
import UIKit


class AlertController {
    
    
    static func showStaticAlert(withMessage message: String, viewController: UIViewController, completion: (() -> Void)? = nil) {
        
        let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        viewController.present(alertController, animated: true, completion: completion)
    }
    
}
