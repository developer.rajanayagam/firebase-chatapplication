//
//  MessageModel.swift
//  Firebase ChatApplication
//
//  Created by raja vijaya kumar on 10/05/19.
//  Copyright © 2019 guvi. All rights reserved.
//

import Foundation

struct MessageModal {
    var name: String
    var chatId: String
    var senderId: String
    var dateAdded: Double
    
    init(name: String, senderId: String) {
        self.name = name
        self.senderId = senderId
        self.chatId = UUID().uuidString
        self.dateAdded = (NSDate().timeIntervalSince1970 * 1000)
    }
    
}
